<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 24-08-16
 * Time: 15:37
 */

namespace JulienCoppin\CustomBundle\Command;


use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Doctrine\ORM\EntityManager;
use JulienCoppin\CustomBundle\CustomType\Bundle;
use JulienCoppin\CustomBundle\Parameter\Generator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Intl\Exception\MissingResourceException;

class ParameterCommand extends ContainerAwareCommand
{
    protected function initialize(InputInterface $input, OutputInterface $output) {
        parent::initialize($input, $output);
    }

    protected function configure()
    {
        $this->setName('juliencoppin:parameter:crud');
        $this->setAliases(array('juliencoppin:parameter:crud'));
        $this->setDescription("Generate the CRUD for a parameter");
        $this->setDefinition(array(
            new InputOption('bundle', '', InputOption::VALUE_REQUIRED, 'The bundle where the entity is located'),
            new InputOption('name', '', InputOption::VALUE_REQUIRED, 'The entity\'s name'),
//            new InputOption('prefix', '', InputOption::VALUE_REQUIRED, 'The prefix for the route'),
        ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');

        $bundleName = $helper->ask($input, $output, new Question('Please enter the bundle where the entity is located : '));
        $name = $helper->ask($input, $output, new Question('Please enter the entity\'s name : '));
//        $prefix = $helper->ask($input, $output, new Question('Please enter the prefix for the route(s) : ', 'blog_parameter'));

        /** @var EntityManager $manager */
        $managerMetadata = new DisconnectedMetadataFactory($this->getContainer()->get('doctrine'));

        $webDir = $this->getApplication()->getKernel()->getRootDir() . '/../web';
        /** @var Bundle $bundle */
        $bundle = $this->getApplication()->getKernel()->getBundle($bundleName);
        $metadataBundle = $managerMetadata->getBundleMetadata($bundle);

        if (!class_exists($metadataBundle->getNamespace() . "\\Entity\\" . $name)) {
            throw new MissingResourceException();
        }

        $metadata = $this->getContainer()->get('doctrine')->getManager()->getClassMetadata($bundle->getName() . ":" . $name);
        $fields = array();

        foreach ($metadata->getFieldNames() as $fieldName) {
            $field = $metadata->getFieldMapping($fieldName);
            if (!array_key_exists('id', $field)) {
                array_push($fields, $field);
            }
        }

        $indexFields = $metadata->getIdentifier();
        for ($i = 0; $i < count($metadata->getFieldNames()); $i++) {
            if (!in_array($metadata->getFieldNames()[$i], $indexFields)) {
                array_push($indexFields, $metadata->getFieldNames()[$i]);
                break;
            }
        }

        Generator::generateFiles($output, $bundle, $name, $fields, $webDir, $indexFields, $metadataBundle->getNamespace());
    }
}