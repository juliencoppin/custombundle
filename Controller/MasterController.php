<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 19-05-16
 */

namespace JulienCoppin\CustomBundle\Controller;

use Doctrine\ORM\EntityManager;
use JulienCoppin\CustomBundle\Exceptions\BreadcrumbException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class MasterController extends Controller
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $container->get('doctrine')->getManager();
        $this->translator = $container->get('translator');

        $this->initFields();
    }

    protected function initFields()
    {
        return null;
    }

    /**
     * @param Request $request
     */
    protected function rejectNonXHR(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('Call should be XHR');
        }
    }

    /**
     * @param $item
     */
    protected function exceptionIfNull($item)
    {
        if (null === $item) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @param array $params
     * @return \WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs
     * @throws BreadcrumbException
     */
    protected function generateBreadcrumbs(array $params)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        foreach ($params as $param) {
            if (!is_array($param)) {
                throw new InvalidTypeException(sprintf("%s is not an array", gettext($param)));
            }
            if (count($param) == 0) {
                throw new BreadcrumbException("Not enought parameters");
            }
            $translation = $this->translator->trans(/** @Ignored */$param[0]);
            switch (count($param)) {
                case 1:
                    $breadcrumbs->addItem($translation);
                    break;
                case 2:
                    $breadcrumbs->addRouteItem($translation, $param[1]);
                    break;
                case 3;
                    $breadcrumbs->addRouteItem($translation, $param[1], $param[2]);
                    break;
                default:
                    throw new BreadcrumbException("Too many parameters");
            }
        }
        return $breadcrumbs;
    }
}