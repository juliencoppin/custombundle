<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 23-05-16
 * Time: 16:54
 */
namespace JulienCoppin\CustomBundle\Controller;

use JulienCoppin\CustomBundle\Entity\SoftDelete;
use JulienCoppin\CustomBundle\Exceptions\InvalidInheritedArgumentException;
use JulienCoppin\CustomBundle\Exceptions\NonUniqueIdentifierException;
use JulienCoppin\CustomBundle\Interfaces\IDeletable;
use JulienCoppin\CustomBundle\Interfaces\IParameterRepository;
use JulienCoppin\CustomBundle\Interfaces\ITemplate;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Exception\NotImplementedException;

abstract class ParameterController extends MasterController implements ITemplate
{
    /**
     * @var string|IParameterRepository
     */
    protected $repo;

    /**
     * @var string
     */
    protected $indexTwig;

    /**
     * @var string
     */
    protected $editTwig;

    /**
     * @var string
     */
    protected $indexRoute;

    /**
     * @var string
     */
    protected $screenName;

    /**
     * @var array
     */
    protected $routeParams;

    /**
     * @var bool
     */
    protected $redirectAfterCreate;

    /**
     * @var string
     */
    protected $updateRoute;

    /**
     * @var string
     */
    protected $emptyObject;

    /**
     * @var string
     */
    protected $formType;

    /**
     * ParameterController constructor.
     */
    public function __construct()
    {
        $this->setRequiredNames();
        $this->validateNames();
    }

    /**
     * @throws InvalidInheritedArgumentException
     */
    protected function initFields()
    {
        $this->repo = $this->em->getRepository($this->repo);

        if (!($this->repo instanceof IParameterRepository)) {
            throw new InvalidInheritedArgumentException($this->repo, IParameterRepository::class);
        }
    }

    public function validateNames()
    {
        $names = array(
            "repo", "indexTwig", "editTwig", "indexRoute", "screenName", 'emptyObject', "formType"
        );

        foreach ($names as $i => $name) {
            if (!isset($this->$name)) {
                throw new \InvalidArgumentException(sprintf("Missing argument : %s", $name));
            }
        }

        if (is_object($this->emptyObject)) {
            throw new InvalidTypeException(sprintf("The %s should be writed like : %s::class", get_class($this->emptyObject), get_class($this->emptyObject)));
        }

        if (!isset($this->routeParams) || !is_array($this->routeParams)) {
            $this->routeParams = array();
        }

        if (!isset($this->redirectAfterCreate)) {
            $this->redirectAfterCreate = false;
        } else {
            if (!isset($this->updateRoute)) {
                throw new \InvalidArgumentException("Missing argument : updateRoute");
            }
        }
    }

    public abstract function setRequiredNames();

    protected abstract function initTranslations();

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $items = $this->repo->findAllWithRequiredData();

        $this->generateBreadcrumbs(array(
            array(sprintf("%s.list", $this->screenName))
        ));

        return $this->render($this->indexTwig, array(
            'items' => $items
        ));
    }

    /**
     * @param Request $request
     * @param $itemID
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws NonUniqueIdentifierException
     */
    public function editAction(Request $request, $itemID)
    {
        $item = $this->repo->findOneWithRequiredData($itemID);
        $isAdd = false;

        if ($item === null) {
            $item = new $this->emptyObject;
            $isAdd = true;
        }

        $form = $this->createForm($this->formType, $item);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($isAdd) {
                $this->em->persist($item);
            }
            $this->em->flush();
            if ($this->redirectAfterCreate && $isAdd) {
                $this->indexRoute = $this->updateRoute;
                $metadata = $this->em->getClassMetadata($this->emptyObject);
                if (count($metadata->getIdentifier()) != 1) {
                    throw new NonUniqueIdentifierException($this->emptyObject);
                }
                $getIdentifier = sprintf("get%s", ucfirst($metadata->getIdentifier()[0]));
                $this->routeParams["itemID"] = $item->$getIdentifier();
            }
            return $this->redirectToRoute($this->indexRoute, $this->routeParams);
        }

        $this->generateBreadcrumbs(array(
            array(sprintf("%s.list", $this->screenName), $this->indexRoute, $this->routeParams),
            array(sprintf("%s.%s", $this->screenName, $isAdd ? "add" : "edit"))
        ));

        return $this->render(sprintf($this->editTwig, $isAdd ? "add" : "edit"), array(
            'form' => $form->createView(), 'item' => $item
        ));
    }

    /**
     * @param Request $request
     * @param $itemID
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws InvalidInheritedArgumentException
     */
    public function removeAction(Request $request, $itemID)
    {
        $item = $this->repo->findForDelete($itemID);

        $this->exceptionIfNull($item);

        if (!($item instanceof IDeletable)) {
            throw new InvalidInheritedArgumentException($item, IDeletable::class);
        }

        $response = array();

        if ($item->isDeletable()) {
            if ($item instanceof SoftDelete) {
                $item->callbackDelete($this->getUser());
            } else {
                $this->em->remove($item);
            }
            $response["deleted"] = true;
            $this->em->flush();
        } else {
            $this->addFlash('danger', $this->translator->trans(/** @Ignore */sprintf("%s.remove.error", $this->screenName)));
            $response["deleted"] = false;
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse($response);
        }

        return $this->redirectToRoute($this->indexRoute, $this->routeParams);
    }
}