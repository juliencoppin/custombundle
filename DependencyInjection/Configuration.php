<?php

namespace JulienCoppin\CustomBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('julien_coppin_custom');

        $rootNode
            ->children()
                ->arrayNode('menu_builder')
                    ->children()
                        ->scalarNode("root_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("li_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("ul_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("active_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("parent_active_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("sub_class")
                            ->isRequired()
                        ->end()
                        ->scalarNode("ul_children_class")
                            ->isRequired()
                        ->end()
                            ->scalarNode("role_namespace")
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('kernel')
                ->end()
            ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
