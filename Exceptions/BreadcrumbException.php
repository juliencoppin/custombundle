<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 07-11-16
 * Time: 10:51
 */

namespace JulienCoppin\CustomBundle\Exceptions;


class BreadcrumbException extends \Exception
{
    /**
     * BreadcrumbException constructor.
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}