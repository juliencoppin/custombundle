<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 31-10-16
 * Time: 14:28
 */

namespace JulienCoppin\CustomBundle\Exceptions;


class InvalidInheritedArgumentException extends \Exception
{
    /**
     * InvalidInheritedArgumentException constructor.
     * @param string $class
     * @param int $toImplement
     */
    public function __construct($class, $toImplement)
    {
        if (!is_object($class)) {
            $class = new $class();
        }
        parent::__construct(sprintf("%s does not implement/inherited %s", get_class($class), $toImplement));
    }
}