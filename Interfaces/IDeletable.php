<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 31-10-16
 * Time: 14:41
 */

namespace JulienCoppin\CustomBundle\Interfaces;


interface IDeletable
{
    /**
     * @return bool
     */
    public function isDeletable();
}