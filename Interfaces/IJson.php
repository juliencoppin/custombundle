<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-05-16
 * Time: 15:24
 */

namespace JulienCoppin\CustomBundle\Interfaces;


interface IJson
{
    /**
     * @return array
     */
    public function ToJson();
}