<?php

namespace JulienCoppin\CustomBundle\Interfaces;

interface IParameterRepository
{
    public function findAllWithRequiredData();

    public function findOneWithRequiredData($itemID);

    public function findForDelete($itemID);
}