<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 19-05-16
 */

namespace JulienCoppin\CustomBundle\Interfaces;

interface ITemplate
{
    public function __construct();

    public function validateNames();

    public function setRequiredNames();
}