<?php
/**
 * Created by PhpStorm.
 * User: Hard
 * Date: 01-07-16
 * Time: 23:25
 */

namespace JulienCoppin\CustomBundle\Menu;


class Menu
{
    /**
     * @var Menu
     */
    private $parent;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Menu[]
     */
    private $children;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $route;

    /**
     * @var string
     */
    private $id;

    /**
     * @var bool
     */
    private $subActive;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var array
     */
    private $subRoutes;

    /**
     * Menu constructor.
     * @param Menu|null $parent
     */
    public function __construct(Menu $parent = null)
    {
        $this->parent = $parent;
        $this->url = "javascript:void(0)";
        $this->active = false;
        $this->subActive = false;
        $this->subRoutes = array();
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    /**
     * @return Menu
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Menu $parent
     */
    public function setParent($parent)
    {
        $parent->addChildren($this);

        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return Menu[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Menu $child
     */
    public function addChildren(Menu $child)
    {
        $this->children[] = $child;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return boolean
     */
    public function isSubActive()
    {
        return $this->subActive;
    }

    /**
     * @param boolean $subActive
     */
    public function setSubActive($subActive)
    {
        $this->subActive = $subActive;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return array
     */
    public function getSubRoutes()
    {
        return $this->subRoutes;
    }

    /**
     * @param string $subRoute
     */
    public function addSubRoutes($subRoute)
    {
        $this->subRoutes[] = $subRoute;
    }

    /**
     * @param array $subRoutes
     */
    public function setSubRoutes($subRoutes)
    {
        $this->subRoutes = $subRoutes;
    }
}