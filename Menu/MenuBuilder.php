<?php
/**
 * Created by PhpStorm.
 * User: Hard
 * Date: 01-07-16
 * Time: 23:03
 */

namespace JulienCoppin\CustomBundle\Menu;


use JulienCoppin\CustomBundle\Exceptions\InvalidInheritedArgumentException;
use JulienCoppin\CustomBundle\Interfaces\IRole;
use JulienCoppin\CustomBundle\Service\Configuration\MenuBuilderParameter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Intl\Exception\MissingResourceException;
use Symfony\Component\Intl\Exception\NotImplementedException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Yaml\Yaml;

class MenuBuilder extends \Twig_Extension
{
    /**
     * @var string
     */
    private $route;

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var MenuBuilderParameter
     */
    private $options;

    /**
     * @var \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var \Symfony\Component\HttpKernel\KernelInterface
     */
    private $kernel;

    /**
     * MenuBuilder constructor.
     * @param RequestStack $requestStack
     * @param Router $router
     * @param AuthorizationChecker $authorizationChecker
     * @param MenuBuilderParameter $options
     * @param KernelInterface $kernel
     */
    public function __construct(RequestStack $requestStack, Router $router, AuthorizationChecker $authorizationChecker, MenuBuilderParameter $options, KernelInterface $kernel)
    {
        $this->request = $requestStack;
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->options = $options;
        $this->kernel = $kernel;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'menu_builder_extension';
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('custom_menu_render', array($this, 'render'), array("is_safe" => array("html")))
        );
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function render()
    {
        $this->route = $this->request->getCurrentRequest()->attributes->get('_route');
        if ($this->route === null && $this->request->getCurrentRequest()->attributes->get('_forwarded') !== null) {
            $this->route = $this->request->getCurrentRequest()->attributes->get('_forwarded')->get('_route');
        } else if ($this->route == null) {
            throw new \Exception();
        }
        $data = $this->parseYAML();
        $menu = new Menu();
        $this->buildMenu($menu, $data);
        $this->findRoute($menu);

        return $this->renderMenu($menu);
    }

    /**
     * @return array
     * @throws \JulienCoppin\CustomBundle\Exceptions\InvalidInheritedArgumentException
     */
    private function parseYAML()
    {
        $path = join(DIRECTORY_SEPARATOR, array($this->kernel->getRootDir(), "config", "menu.yml"));
        if (!file_exists($path)) {
            throw new MissingResourceException(sprintf("The file %s (%s) does not exists", "menu.yml", $path));
        }
        if (!class_exists($this->options->getRoleNamespace())) {
            throw new NotImplementedException(sprintf("The class %s does not exist !", $this->options->getRoleNamespace()));
        }
        if (!isset(class_implements($this->options->getRoleNamespace())[IRole::class])) {
            throw new InvalidInheritedArgumentException($this->options->getRoleNamespace(), IRole::class);
        }
        $data = Yaml::parse(file_get_contents($path));
        foreach ($data as $role => $value) {
            $const = sprintf("%s::%s", $this->options->getRoleNamespace(), strtoupper($role));
            $ROLE = defined($const) ? constant($const) : null;
            if ($ROLE !== null && $this->authorizationChecker->isGranted($ROLE)) {
                return is_array($value) ? $value : array();
            }
        }
        return isset($data["COMMON"]) ? (is_array($data["COMMON"]) ? $data["COMMON"] : array()) : array();
    }

    /**
     * @param Menu $menu
     * @return string
     */
    private function renderMenu(Menu $menu)
    {
        $res = "";
        $parent = $menu->getParent();
        if (null === $parent) {
            $res .= "<ul class='" . $this->options->getRootClass() . "%s'>";
        } else {
            $res .= "<li class='" . $this->options->getLiClass() . "%s'>";
            $class = sprintf("%s%s%s", $menu->isActive() ? " " . $this->options->getActiveClass() : null, $menu->isSubActive() ? " " . $this->options->getParentActiveClass() : null, "%s");
            $res = sprintf($res, $class);
            $res .= sprintf("<a href='%s'>%s<span class='title'>&nbsp;%s</span>%s%s</a>",
                $menu->getUrl(),
                $menu->getIcon() !== null ? sprintf("<i class='%s'></i>", $menu->getIcon()) : null,
                $menu->getLabel(),
                $menu->hasChildren() ? "<i class='icon-arrow'></i>" : null,
                '<span class=\'selected\'></span>'
            );
        }

        if ($menu->hasChildren()) {
            $res = sprintf($res, $parent !== null ? " " . $this->options->getUlChildrenClass() : null);
            $resChildren = $parent !== null ? "<ul class='" . $this->options->getUlClass() . "'>" : "";

            foreach ($menu->getChildren() as $child) {
                $resChildren .= $this->renderMenu($child);
            }
            if ($menu->getParent() !== null) {
                $resChildren .= "</ul>";
            }
            $res .= $resChildren;
        } else {
            $res = sprintf($res, null);
        }

        if (null === $parent) {
            $res .= "</ul>";
        } else {
            $res .= "</li>";
        }
        return $res;
    }

    /**
     * @param Menu $menu
     * @param $builderData
     */
    private function buildMenu(Menu $menu, $builderData)
    {
        foreach ($builderData as $value => $data) {
            if (array_key_exists('hidden', $data)) {
                continue;
            }
            if (array_key_exists('roles', $data)) {
                if (!$this->authorizationChecker->isGranted($data['roles'])) {
                    continue;
                }
            }
            $sub_menu = new Menu($menu);
            if (array_key_exists('route', $data)) {
                $sub_menu->setRoute($data['route']);
                $sub_menu->setUrl($this->router->generate($data['route']));
            }
            if (array_key_exists('class', $data)) {
                $sub_menu->setClass($data['class']);
            }
            if (array_key_exists('label', $data)) {
                $sub_menu->setLabel($data['label']);
            }
            if (array_key_exists('icon', $data)) {
                $sub_menu->setIcon($data['icon']);
            }
            if (array_key_exists('subRoutes', $data)) {
                $sub_menu->setSubRoutes($data['subRoutes']);
            }
            $sub_menu->setParent($menu);
            if (array_key_exists('children', $data)) {
                $this->buildMenu($sub_menu, $data['children']);
            }
        }
    }

    /**
     * @param Menu $menu
     * @param int $depth
     */
    private function setActive(Menu $menu, $depth = 0)
    {
        $menu->setActive(true);
        if ($depth != 0) {
            $menu->setSubActive(true);
        }
        if ($menu->getParent() !== null) {
            $this->setActive($menu->getParent(), $depth+1);
        }
    }

    /**
     * @param Menu $menu
     * @return bool
     */
    private function findRoute(Menu $menu)
    {
        if ($menu->getRoute() === $this->route) {
            $this->setActive($menu);
            return true;
        }

        if (count($menu->getSubRoutes()) > 0) {
            if (in_array($this->route, $menu->getSubRoutes())) {
                $this->setActive($menu);
                return true;
            }
        }

        if ($menu->hasChildren()) {
            foreach ($menu->getChildren() as $child) {
                if ($this->findRoute($child)) {
                    return true;
                }
            }
        }
        return false;
    }
}