<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 08-09-16
 * Time: 14:06
 */

namespace JulienCoppin\CustomBundle\Parameter;


use JulienCoppin\CustomBundle\CustomType\Bundle;
use JulienCoppin\CustomBundle\Helpers\Inflect;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Yaml\Yaml;

class Generator
{
    /**
     * @var Bundle
     */
    private static $bundle;

    private static $name;

    private static $prefix;

    /** @var Filesystem */
    private static $filesystem;

    /** @var OutputInterface */
    private static $output;

    private static $webDir;

    private static $options;

    private static $bundleNamespace;

    public static function generateFiles(OutputInterface $output, $bundle, $name, array $fields, $webDir, $indexFields, $bundleNamespace)
    {
        self::$output = $output;
        self::$bundle = $bundle;
        self::$name = ucfirst($name);
        self::$prefix = str_replace("bundle", "", strtolower(self::$bundle->getName()));
        if (strpos(self::$prefix, "main") !== false) {
            self::$prefix = str_replace("main", "", self::$prefix) . "_main";
        }
        self::$filesystem = new Filesystem();
        self::$webDir = $webDir;
        self::$bundleNamespace = $bundleNamespace;

        self::generateDefaultValues();

        self::generateController(self::$options);
        self::generateRouting(self::$options);
        self::generateRepository(self::$options, $indexFields[0]);
        self::generateIndexTwig(self::$options, $indexFields);
        self::generateUpdateTwig(self::$options);
        self::generateForm(self::$options, $fields);
//        self::$output->writeln("<info>Do not forget to modify the files for your needs</info>");
    }

    private static function generateDefaultValues()
    {
        self::$options = array(
            "indexRoute" => self::$prefix . "_parameter_" . strtolower(self::$name) . "_index",
            "updateRoute" => self::$prefix . "_parameter_" . strtolower(self::$name) . "_update",
            "deleteRoute" => self::$prefix . "_parameter_" . strtolower(self::$name) . "_remove",
            "entityNamespace" => self::$bundleNamespace . '\\Entity',
            "entityClass" => self::$name,
            "object" => self::$name,
            "twigLayout" => sprintf("%s::layout.html.twig", self::$bundle->getName()),
            "pluralName" => Inflect::pluralize(strtolower(self::$name)),
            "screenName" => strtolower(self::$name),
            "formName" => strtolower(self::$name) . "_form",
            "formNamespace" => self::$bundleNamespace . "\\Form\\Parameter",
            "formClass" => self::$name . "FormType",

        );
    }

    private static function generateController(array $options)
    {
        $namespace = self::$bundleNamespace . '\\Controller\\Parameter';
        $className = self::$name . 'Controller';

        $repo = self::$bundle->getName() . ":" . self::$name;
        $editTwig = self::$bundle->getName() . ":Parameter/" . self::$name . ":%s" . self::$name . ".html.twig";
        $indexTwig = sprintf($editTwig, "index");
        $emptyObject = self::$name;

        self::$output->writeln(
            sprintf("Generating controller <info>%s</info>", $className)
        );

        $content = <<<EOT
<?php
        
namespace $namespace;

use JulienCoppin\CustomBundle\Controller\ParameterController;
use {$options["entityNamespace"]}\\{$options["entityClass"]};
use {$options["formNamespace"]}\\{$options["formClass"]};

class $className extends ParameterController
{
    public function setRequiredNames()
    {
        \$this->repo = "$repo";
        \$this->indexTwig = "$indexTwig";
        \$this->editTwig = "$editTwig";
        \$this->indexRoute = "{$options["indexRoute"]}";
        \$this->screenName = "{$options["screenName"]}";
        \$this->emptyObject = $emptyObject::class;
        \$this->formType = {$options["formClass"]}::class;
    }

    protected function initTranslations()
    {   
        \$this->translator->trans("{$options["screenName"]}.list");
        \$this->translator->trans("{$options["screenName"]}.add");
        \$this->translator->trans("{$options["screenName"]}.edit");
    }
}
EOT;

        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$bundle->getPath(), "Controller", "Parameter", $className . ".php",
                )
            ),
            $content
        );
    }

    private static function generateRepository(array $options, $id)
    {
        $namespace = self::$bundleNamespace . '\\Repository';
        $className = self::$name . 'Repository';
        $alias = strtolower(str_split(self::$name)[0]);

        self::$output->writeln(
            sprintf("Generating repository <info>%s</info>", $className)
        );

        $content = <<<EOL
<?php

namespace $namespace;

use Doctrine\ORM\EntityRepository;
use JulienCoppin\CustomBundle\Interfaces\IParameterRepository;
use {$options["entityNamespace"]}\\{$options["entityClass"]};

class $className extends EntityRepository implements IParameterRepository
{
    /**
     * @return {$options["object"]}[]
     */
    public function findAllWithRequiredData()
    {
        \$qb = \$this->createQueryBuilder("$alias");
        
        return \$qb->getQuery()->getResult();
    }

    /**
     * @param int \$itemID
     * @return {$options["object"]}
     */
    public function findOneWithRequiredData(\$itemID)
    {
        \$qb = \$this->createQueryBuilder("$alias")
            ->where("$alias.$id = :itemID")
            ->setParameter("itemID", \$itemID);
            
        return \$qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int \$itemID
     * @return {$options["object"]}
     */
    public function findForDelete(\$itemID)
    {
        \$qb = \$this->createQueryBuilder("$alias")
            ->where("$alias.$id = :itemID")
            ->setParameter("itemID", \$itemID);
            
        return \$qb->getQuery()->getOneOrNullResult();
    }
}
EOL;
        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$bundle->getPath(), "Repository", $className . ".php",
                )
            ),
            $content
        );
    }

    private static function generateRouting(array $options)
    {
        self::$output->writeln(sprintf("Updating routing file <info>parameters.yml</info>"));

        $configFolder = join(
            DIRECTORY_SEPARATOR,
            array(self::$bundle->getPath(), "Resources", "config")
        );
        $mainRouting = join(
            DIRECTORY_SEPARATOR,
            array($configFolder, "routing.yml")
        );
        $parameterRouting = join(
            DIRECTORY_SEPARATOR,
            array($configFolder, "parameters.yml")
        );

        $mainParameterRoute = strtolower(
                self::$prefix
            ) . "_parameters";

        if (!self::$filesystem->exists($mainRouting)) {
            throw new ResourceNotFoundException("Missing routing.yml file");
        }

        $mainRoutingContent = Yaml::parse(file_get_contents($mainRouting));

        $mainRoutingContent[$mainParameterRoute] = array(
            'resource' => 'parameters.yml',
            'prefix' => '/admin/parameters',
        );
        self::$filesystem->dumpFile(
            $mainRouting,
            Yaml::dump($mainRoutingContent)
        );

        $content = array();
        if (self::$filesystem->exists($parameterRouting)) {
            $content = Yaml::parse(file_get_contents($parameterRouting));
        }

        $controller = self::$bundle->getName() . ":Parameter/" . self::$name . ':';

        $routes = array(
            $options["indexRoute"] => array(
                'path' => '/' . $options["pluralName"],
                'defaults' => array(
                    '_controller' => $controller . 'index',
                ),
                'methods' => array(
                    'GET',
                ),
            ),
            $options["updateRoute"] => array(
                'path' => '/' . $options["pluralName"] . '/{itemID}',
                'defaults' => array(
                    '_controller' => $controller . 'edit',
                ),
                'requirements' => array(
                    'itemID' => '\d*|-1',
                ),
                'methods' => array(
                    'GET',
                    'POST',
                ),
            ),
            $options["deleteRoute"] => array(
                'path' => '/' . $options["pluralName"] . '/{itemID}/remove',
                'defaults' => array(
                    '_controller' => $controller . 'remove',
                ),
                'requirements' => array(
                    'itemID' => '\d*',
                ),
                'methods' => array(
                    'POST',
                ),
                'options' => array(
                    'expose' => true,
                ),
            ),
        );
        // TODO If $content is not an array
        $content = array_merge($content, $routes);
        self::$filesystem->dumpFile($parameterRouting, Yaml::dump($content));
    }

    private static function generateIndexTwig(array $options, $indexFields)
    {
        $deleteClass = "delete" . self::$name;
        $modal = sprintf("modal%s", ucfirst($deleteClass));
        $js = sprintf("index%s.js", self::$name);

        $content = <<<EOL
{% extends "{$options["twigLayout"]}" %}

{% block title %}
    {{ '{$options["pluralName"]}'|trans }} - {{ parent() }}
{% endblock %}

{% block stylesheets %}
    {{ parent() }}
    <link rel="stylesheet" href="{{ asset("assets/clip-one/plugins/DataTables/dataTables.bootstrap.min.css") }}">
    <link href="{{ asset("assets/clip-one/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("assets/clip-one/plugins/bootstrap-modal/css/bootstrap-modal.css") }}" rel="stylesheet" type="text/css"/>
{% endblock %}

{% block content %}
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">{{ '{$options["pluralName"]}'|trans }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{'{$options["screenName"]}.list' | trans}}
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>{{ 'TO_EDIT' | trans }}</th>
                            <th>{{ 'actions' | trans }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% if items|length > 0 %}
                            {% for item in items %}
                                <tr data-id="{{ item.{$indexFields[0]} }}">
                                    <td>{{ item.{$indexFields[1]} }}</td>
                                    <td class="center">
                                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                                            <a href="{{ path('{$options["updateRoute"]}', {'itemID' : item.{$indexFields[0]}}) }}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="{{ 'edit'|trans }}"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-bricky tooltips $deleteClass" data-placement="top" data-original-title="{{ 'delete'|trans }}"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            {% endfor %}
                        {% endif %}
                        </tbody>
                    </table>
                    <table class="buttonline col-md-12 ">
                        <tbody>
                        <tr>
                            <td align="left">
                                <a href="{{ path('{$options["updateRoute"]}', {'itemID' : -1}) }}" class="btn btn-default">{{ 'add' | trans }}</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {% include 'JulienCoppinCustomBundle::modal.html.twig' with {
        'modalID' : '$modal', 'title' : '{$options["screenName"]}.title'|trans, 'message' : '{$options["screenName"]}.remove.message'|trans, 'messageModalID' : '{$modal}Message'
    } %}

{% endblock %}

{% block js %}
    {{ parent() }}
    <script type="text/javascript" src="{{ asset("assets/clip-one/plugins/bootstrap-modal/js/bootstrap-modal.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/clip-one/plugins/bootstrap-modal/js/bootstrap-modalmanager.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/clip-one/plugins/DataTables/jquery.dataTables.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/clip-one/plugins/DataTables/dataTables.bootstrap.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("bundles/juliencoppincustom/package.js") }}"> </script>
    <script type="text/javascript" src="{{ asset("js/main.js") }}"> </script>
    <script type="text/javascript" src="{{ asset("js/Parameter/$js") }}"> </script>
    <script type="text/javascript" src="{{ asset('bundles/fosjsrouting/js/router.js') }}"></script>
    <script type="text/javascript" src="{{ path('fos_js_routing_js', {'callback': 'fos.Router.setData'}) }}"></script>
{% endblock %}
EOL;

        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$bundle->getPath(),
                    "Resources",
                    "views",
                    "Parameter",
                    self::$name,
                    "index" . self::$name . ".html.twig",
                )
            ),
            $content
        );
        self::$output->writeln(sprintf("Generating twig file <info>%s</info>", "index" . self::$name));

        $jsContent = <<<EOL
$(document).ready(function() {
    var modalDelete = $('#$modal');
    if (modalDelete.length != 1) {
        return;
    }
    var elemID = null;
    modalDelete.find("modal-footer a:first").on('click', function() {
        if (elemID === null) {
            return;
        }
        modalDelete.modal("hide");
        ajax({
            url: Routing.generate('{$options["deleteRoute"]}', {'itemID' : elemID}),
            type: 'POST',
            modal: modalDelete
        });
    });
    var message = $('#{$modal}Message');
    $('.$deleteClass').on('click', function() {
        var tr = $(this).parents('tr');
        elemID = tr.data("id");
        message.text(tr.find('tr:first').text());
        modalDelete.modal("show");
    });
});
EOL;

        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$webDir,
                    "js",
                    "Parameter",
                    $js
                )
            ),
            $jsContent
        );
        self::$output->writeln(sprintf("Generating javascript file <info>%s</info>", $js));
    }

    private static function generateUpdateTwig(array $options)
    {
        $pages = array("add", "edit");
        $form = sprintf("%s:Parameter/%s:form%s.html.twig", self::$bundle->getName(), self::$name, self::$name);
        $js = sprintf("update%s.js", self::$name);

        foreach ($pages as $page) {
            $content = <<<EOL
{% extends "{$options["twigLayout"]}" %}

{% block title %}
    {{ '{$options["pluralName"]}'|trans }} - {{ parent() }}
{% endblock %}

{% block content %}
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header">{{ '{$options["screenName"]}.$page' |trans }}</h1>
        </div>
    </div>
    <div class="row">
        {% include "$form" %}
    </div>
    {% include "JulienCoppinCustomBundle::cancel.html.twig" with {'route' : path('{$options["indexRoute"]}')} %}

{% endblock %}

{% block js %}
    {{ parent() }}
    <script type="text/javascript" src="{{ asset("bundles/juliencoppincustom/package.js") }}"> </script>
    <script type="text/javascript" src="{{ asset("js/Parameter/$js") }}"> </script>
{% endblock %}
EOL;

            self::$filesystem->dumpFile(
                join(
                    DIRECTORY_SEPARATOR,
                    array(
                        self::$bundle->getPath(),
                        "Resources",
                        "views",
                        "Parameter",
                        self::$name,
                        $page . self::$name . ".html.twig",
                    )
                ),
                $content
            );
            self::$output->writeln(sprintf("Generating twig file <info>%s</info>", $page . self::$name));
        }
        $jsContent = <<<EOL
formValidation("{$options["formName"]}");
EOL;
        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$webDir,
                    "js",
                    "Parameter",
                    $js,
                )
            ),
            $jsContent
        );
        self::$output->writeln(sprintf("Generating javascript file <info>%s</info>", $js));
    }

    private static function generateForm(array $options, $fields)
    {
        /*
                $formTypeData = "";
                $formViewData = "";

                $usings = array();

                foreach ($fields as $field) {
                    $opts = array(
                        "attr" => "array()",
                        "required" => $field["nullable"] ? "false" : "true",
                        "label" => $field["fieldName"]
                    );

                    switch ($field["type"]){
                        case "boolean":
                            if (!in_array("CheckboxType", $usings)) {
                                $usings[] = "CheckboxType";
                            }
                            $opts["type"] = "CheckboxType::class";
                            break;
                        case "string":
                            if (!in_array("TextType", $usings)) {
                                $usings[] = "TextType";
                            }
                            $opts["type"] = "TextType::class";
                            $opts["attr"] = "array('class' => 'form-control')";
                            break;
                        case "text":
                            if (!in_array("TextareaType", $usings)) {
                                $usings[] = "TextareaType";
                            }
                            $opts["type"] = "TextareaType::class";
                            $opts["attr"] = "array('class' => 'form-control')";
                            break;
                        default:
                            continue;
                            break;
                    }
                    $formTypeData .= <<<EOL
                \$builder->add("{$field["fieldName"]}", {$opts["type"]}, array(
                    'attr' => {$opts["attr"]},
                    'label' => "{$opts["label"]}",
                    'label_attr' => array('class' => 'control-label col-md-3'),
                    'required' => {$opts["required"]}
                ));


        EOL;
                    $formViewData .= self::generateTWIG($field["fieldName"], $field["type"]);
                }

                $using = "";
                foreach ($usings as $item) {
                    $using .= 'use Symfony\Component\Form\Extension\Core\Type\\' . $item . ";\n";
                }
        */
        $contentFormType = <<<EOL
<?php

namespace {$options["formNamespace"]};

use JulienCoppin\CustomBundle\Form\CustomSaveFormType;
use Symfony\Component\Form\FormBuilderInterface;

class {$options["formClass"]} extends CustomSaveFormType
{
    
    public function setRequiredNames()
    {
        \$this->data_class = '{$options["entityNamespace"]}\\{$options["entityClass"]}';
        \$this->name = '{$options["formName"]}';
    }
    
    /**
     * @param FormBuilderInterface \$builder
     * @param array \$options
     */
    public function buildForm(FormBuilderInterface \$builder, array \$options)
    {
        parent::buildForm(\$builder, \$options);        
    }
}
EOL;
        $contentFormView = <<<EOL
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ '{$options["pluralName"]}.details' |trans }}
        </div>
        <div class="panel-body">
            {{ form_start(form) }}
            {{ form_errors(form) }}
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="col-md-offset-9 col-md-3">
                        {{ form_widget(form.save) }}
                    </div>
                </div>
            </div>
            {{ form_end(form) }}
        </div>
    </div>
</div>

EOL;

        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$bundle->getPath(),
                    "Form",
                    "Parameter",
                    $options["formClass"] . ".php",
                )
            ),
            $contentFormType
        );
        self::$output->writeln(sprintf("Generating form type <info>%s</info>", $options["formClass"]));

        self::$filesystem->dumpFile(
            join(
                DIRECTORY_SEPARATOR,
                array(
                    self::$bundle->getPath(),
                    "Resources",
                    "views",
                    "Parameter",
                    self::$name,
                    "form" . self::$name . ".html.twig",
                )
            ),
            $contentFormView
        );
        self::$output->writeln(sprintf("Generating twig file <info>%s</info>", "form" . self::$name));
    }

    /**
     * @param $name
     * @param $type
     * @return string
     */
    private static function generateTWIG($name, $type)
    {
        $twig = "";
        switch ($type) {
            case "boolean":
                $twig .= <<<EOL
                <div class="form-group col-md-12 {% if form.$name.vars.errors|length > 0 %}has-error{% endif %}">
                    <div class="col-md-offset-3 col-md-9">
                        <div class="checkbox">
                            <label>
                                {{ form_widget(form.$name) }}&nbsp;{{ form.$name.vars.label }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9 validation-error">
                        {{ form_errors(form.$name) }}
                    </div>
                </div>

EOL;
                break;
            case "string":
            case "text":
                $twig .= <<<EOL
                <div class="form-group col-md-12 {% if form.$name.vars.errors|length > 0 %}has-error{% endif %}">
                    {{ form_label(form.$name) }}
                    <div class="col-md-9">
                        {{ form_widget(form.$name) }}
                    </div>
                    <div class="col-md-offset-3 col-md-9 validation-error">
                        {{ form_errors(form.$name) }}
                    </div>
                </div>

EOL;
                break;
            default:
                break;
        }
        return $twig;
    }
}