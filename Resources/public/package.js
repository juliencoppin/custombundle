/**
 * Created by Julien on 02-11-16.
 */
if (typeof jQuery === 'undefined') {
    throw new Error('This bundle requires jQuery');
}

var ERROR_CLASS = "has-error";
var HIDDEN_CLASS = "hidden";
var INPUT_SELECTOR = "input:not([type='button'], [type='hidden'], [type='submit'])";

jQuery.fn.tagName = function() {
    return this.prop("tagName").toLowerCase();
};

var loader = {
    modal   : null,
    item    : null,
    html    : false,
    visible : false,
    isError : function() {
        if (this.modal === null || this.modal === undefined || typeof this.modal !== "object"
            || this.item === null || this.item === undefined || typeof this.item !== "object") {
            throw "Unexpected error";
        }
    },
    show    : function() {
        this.generate();
        if (!this.visible) {
            this.item.append($("<div class='loader'></div>"));
            this.modal.modal("show");
            this.visible = true;
        }
    },
    hide    : function() {
        this.generate();
        if (this.visible) {
            this.modal.modal("hide");
            this.item.html("");
            this.visible = false;
        }
    },
    generate: function () {
        if (this.html) {
            return;
        }
        this.modal = $('<div id="modal-loader" class="" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;"><div class="modal-loader-body"></div></div>');
        $(document.body).append(this.modal);
        this.item = this.modal.find(".modal-loader-body");
        this.html = true;
        this.isError();
    }
};

var customModal = {
    modal   : null,
    item    : null,
    html    : false,
    visible : false,
    isError : function() {
        if (this.modal === null || this.modal === undefined || typeof this.modal !== "object"
            || this.item === null || this.item === undefined || typeof this.item !== "object") {
            throw "Unexpected error";
        }
    },
    show    : function(message) {
        this.generate();
        if (!this.visible) {
            this.item.append($(message));
            this.modal.modal("show");
            this.visible = true;
        }
    },
    showError: function () {
        this.generate();
        if (!this.visible) {
            this.item.append($("<p class='text-danger'><strong>An error has occured.</strong> Reload the page and try again.</p>"));
            this.modal.find(".modal-title").html("<h4>Error !!!</h4>");
            this.modal.find(".modal-footer button").val("Close");
            this.modal.modal("show");
            this.visible = true;
        }
    },
    hide    : function() {
        this.generate();
        if (this.visible) {
            this.modal.modal("hide");
            this.item.html("");
            this.visible = false;
        }
    },
    generate: function () {
        if (this.html) {
            return;
        }
        this.modal = $("<div class='modal fade in' style='width: 760px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><div class='modal-title'>Title</div></div><div class='modal-body'><div class='row'><div class='col-md-12' id='modal-block-content'></div></div></div><div class='modal-footer'><button type='button' data-dismiss='modal' class='btn btn-primary'>Close</button></div></div>");
        $(document.body).append(this.modal);
        this.item = this.modal.find(".modal-body #modal-block-content");
        this.html = true;
        this.isError();
    }
};

function formValidation(name) {
    var form = $("form[name='" + name + "']");
    if (form.length == 0) {
        return;
    }
    form.submit(function(e) {
        if (!analyseInputs(this)) {
            e.preventDefault();
        }
    });
    form.find(INPUT_SELECTOR).each(function() {
        addListener($(this));
    });
    form.find("textarea").each(function() {
        addListener($(this));
    });
}

/**
 * @param form
 * @returns {boolean}
 */
function analyseInputs(form) {
    var valid = true;
    $(form).find(INPUT_SELECTOR).each(function() {
        if (!updateInput(this)) {
            valid = false;
        }
    });
    $(form).find("textarea").each(function() {
        if (!updateInput(this)) {
            valid = false;
        }
    });
    return valid && form.checkValidity();
}

/**
 * @param input Add an listener to the input
 */
function addListener(input) {
    var event = null;
    switch (input.tagName()) {
        case "textarea":
            break;
        case "input":
            switch (input.attr("type")) {
                case "checkbox":
                case "radio":
                case "date":
                    break;
                case "submit":
                    event = null;
                    break;
                default:
                    event = "input";
                    break;
            }
            break;
        default:
            throw "The type " + input.tagName() + " is not recognized";
    }
    if (event !== null) {
        input.on(event, updateInput);
    }
}

/**
 * @param elem Input to update
 * @returns {boolean}
 */
function updateInput(elem) {
    if (elem === undefined) {
        elem = this;
    }
    var parent = $(elem).parents(".form-group");
    if (parent.length == 1) {
        var isValid = elem.checkValidity();
        updateClass(parent, ERROR_CLASS, !isValid);
        var child = parent.find("validation-error");
        if (child.length == 1) {
            updateClass(child, HIDDEN_CLASS, isValid);
        }
        return isValid;
    }
    return true;
}

/**
 * @param elem Element to apply the changes to
 * @param name The class name(s)
 * @param state If defined, determine if the class must be add (true) or removed (false)
 */
function updateClass(elem, name, state) {
    if (state === undefined) {
        elem.toggleClass(name);
    } else {
        elem.toggleClass(name, state);
    }
}


function ajax(opts) {
    $.ajax({
        url     : opts.url,
        type    : opts.type,
        dataType: "JSON",
        beforeSend: function() {
            if (opts.modal !== undefined) {
                opts.modal.modal("hide");
            }
            loader.show();
        },
        success : function(data) {
            if (opts.callback !== undefined) {
                opts.callback(data);
            } else {
                window.location.reload();
            }
        },
        error   : function() {
            loader.hide();
            customModal.showError();
        }
    });
}

function modalValidation(params) {
    if (params === undefined) {
        return;
    }
    var elems = $('.' + params.className);
    elems.each(function () {
        $(this).off('click');
        $(this).on('click', function() {
            var tr = $(this).parents('tr');
            var modal = $('#' + params.modal);

            if (params.routeParams === undefined) {
                params.routeParams = new Object();
            }

            if (params.paramNames !== undefined) {
                for (var param in params.paramNames) {
                    params.routeParams[param] = tr.data(params.paramNames[param]);
                }
            }

            var message = modal.find('#' + params.message);
            var content = "";
            for (var indice in params.content) {
                var data = tr.find("td:eq(" + indice + ")").text();
                content += data;
                if (data !== "") {
                    content += " ";
                }
                content += params.content[indice];
            }
            message.text(content);

            var btn = modal.find(".modal-footer a.btn-validate");
            if (btn.length > 0) {
                btn.off("click");
                btn.on('click', function() {
                    ajax({
                        url: Routing.generate(params.route, params.routeParams),
                        type: "POST",
                        modal: modal
                    });
                });
            }
            modal.modal("show");
        });
    });
}