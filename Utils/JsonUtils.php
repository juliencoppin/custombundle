<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 27-05-16
 * Time: 15:23
 */

namespace JulienCoppin\CustomBundle\Utils;


use JulienCoppin\CustomBundle\Interfaces\IJson;

class JsonUtils
{
    /**
     * @param $data
     * @return array
     */
    public static function ToJson($data)
    {
        $res = array();
        foreach ($data as $item) {
            if ($item instanceof IJson) {
                array_push($res, $item->ToJson());
            }
        }
        return $res;
    }
}